package lt.juodamorka.game.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.GridPoint3;
import com.badlogic.gdx.math.Vector3;

public interface GameObject {
    
    void update();
    
    void initLocation();
    
    void initSize(int sizeX, int sizeY);
    
    void dispose();
    
    Vector3 getLocation();
    
    GridPoint3 getSize();
    
    Texture getTexture();
    
    Sprite getSprite();
}
