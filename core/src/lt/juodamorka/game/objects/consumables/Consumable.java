package lt.juodamorka.game.objects.consumables;

import lt.juodamorka.game.objects.GameObject;

public interface Consumable extends GameObject {
    
    Boolean getConsumed();
    
    void setConsumed(Boolean consumed);
}
