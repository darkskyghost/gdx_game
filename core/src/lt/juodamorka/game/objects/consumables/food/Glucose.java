package lt.juodamorka.game.objects.consumables.food;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import lt.juodamorka.game.controllers.camera.CameraController;

public class Glucose extends Food {
    
    int sizeX = 1;
    int sizeY = 1;
    
    Color color = Color.GREEN;
    
    public Glucose() {
        batch = new SpriteBatch();
        sprite = new Sprite();
        consumed = false;
        cameraController = CameraController.getInstance();
        initLocation();
        initSize(sizeX, sizeY);
        createTexture(color);
    }
}
