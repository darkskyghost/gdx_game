package lt.juodamorka.game.objects.consumables.food;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import lt.juodamorka.game.controllers.camera.CameraController;

public class Protein extends Food {
    
    int sizeX = 2;
    int sizeY = 2;
    
    Color color = Color.BLUE;
    
    public Protein() {
        batch = new SpriteBatch();
        sprite = new Sprite();
        consumed = false;
        cameraController = CameraController.getInstance();
        initLocation();
        initSize(sizeX, sizeY);
        createTexture(color);
    }
}