package lt.juodamorka.game.objects.consumables.food;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.GridPoint3;
import com.badlogic.gdx.math.Vector3;
import lt.juodamorka.game.controllers.camera.CameraController;
import lt.juodamorka.game.objects.consumables.Consumable;
import lt.juodamorka.game.utils.Constants;

import java.util.Random;

public abstract class Food implements Consumable {
    
    SpriteBatch batch;
    
    Texture texture;
    Sprite sprite;
    
    Vector3 location;
    GridPoint3 size;
    Boolean consumed;
    CameraController cameraController;
    
    int sizeX = 1;
    int sizeY = 1;
    
    Color color = Color.WHITE;
    
    public Food() {
        batch = new SpriteBatch();
        sprite = new Sprite();
        consumed = false;
        cameraController = CameraController.getInstance();
        initLocation();
        initSize(sizeX, sizeY);
        createTexture(color);
    }
    
    @Override
    public void update() {
        batch.setProjectionMatrix(cameraController.getCamera().combined);
        batch.begin();
        batch.draw(texture, location.x, location.y);
        batch.end();
    }
    
    void createTexture(Color color) {
        Pixmap pixmap = new Pixmap(size.x, size.y, Pixmap.Format.RGBA8888);
        pixmap.setColor(color);
        pixmap.fill();
        Texture texture = new Texture(pixmap);
        pixmap.dispose();
        
        this.texture = texture;
        sprite.setTexture(texture);
        sprite.setBounds(location.x, location.y, size.x, size.y);
    }
    
    @Override
    public void initLocation() {
        location = randomLocation();
    }
    
    @Override
    public void initSize(int sizeX, int sizeY) {
        size = new GridPoint3();
        size.x = sizeX;
        size.y = sizeY;
    }
    
    @Override
    public void dispose() {
        batch.dispose();
        texture.dispose();
    }
    
    private Vector3 randomLocation() {
        Vector3 vector = new Vector3();
        
        int randomX = new Random().nextInt(2);
        
        if (randomX >= 0 && randomX < 1) {
            vector.x -= new Random().nextInt(Constants.WORLD_WIDTH);
        } else {
            vector.x += new Random().nextInt(Constants.WORLD_WIDTH);
        }
        
        int randomY = new Random().nextInt(2);
        
        if (randomY >= 0 && randomY < 1) {
            vector.y -= new Random().nextInt(Constants.WORLD_HEIGHT);
        } else {
            vector.y += new Random().nextInt(Constants.WORLD_HEIGHT);
        }
        
        return vector;
    }
    
    @Override
    public Vector3 getLocation() {
        return location;
    }
    
    public void setLocation(Vector3 location) {
        this.location = location;
    }
    
    @Override
    public GridPoint3 getSize() {
        return size;
    }
    
    public void setSize(GridPoint3 size) {
        this.size = size;
    }
    
    public Texture getTexture() {
        return texture;
    }
    
    public void setTexture(Texture texture) {
        this.texture = texture;
    }
    
    @Override
    public Sprite getSprite() {
        return sprite;
    }
    
    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }
    
    public Boolean getConsumed() {
        return consumed;
    }
    
    public void setConsumed(Boolean consumed) {
        this.consumed = consumed;
    }
}
