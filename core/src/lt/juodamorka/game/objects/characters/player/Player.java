package lt.juodamorka.game.objects.characters.player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.GridPoint3;
import com.badlogic.gdx.math.Vector3;
import lt.juodamorka.game.controllers.camera.CameraController;
import lt.juodamorka.game.controllers.character.PlayerMovementController;
import lt.juodamorka.game.objects.characters.Character;
import lt.juodamorka.game.objects.consumables.Consumable;
import lt.juodamorka.game.utils.Constants;

public class Player implements Character {
    
    private static Player instance;
    
    private PlayerMovementController playerMovementController;
    private CameraController cameraController;
    
    private SpriteBatch batch;
    
    private Texture texture;
    private Sprite sprite;
    private Vector3 location;
    private GridPoint3 size;
    
    private int shrinkCountdown;
    
    private boolean alive;
    
    int sizeX = 10;
    int sizeY = 10;
    
    private Player() {
        alive = true;
        shrinkCountdown = 0;
        playerMovementController = new PlayerMovementController(this);
        cameraController = CameraController.getInstance();
        batch = new SpriteBatch();
        sprite = new Sprite();
        initLocation();
        initSize(sizeX, sizeY);
        createTexture();
    }
    
    public void initLocation() {
        location = new Vector3();
        location.x = Constants.WORLD_WIDTH / 2;
        location.y = Constants.WORLD_HEIGHT / 2;
    }
    
    public void initSize(int sizeX, int sizeY) {
        size = new GridPoint3();
        size.x = sizeX;
        size.y = sizeY;
    }
    
    public void respawn() {
        alive = true;
        initLocation();
        initSize(sizeX, sizeY);
        createTexture();
    }
    
    @Override
    public void update() {
        playerMovementController.update();
        drawPlayer();
        shrink();
        cameraController.getCamera().position.lerp(location, 10 * Gdx.graphics.getDeltaTime());
    }
    
    private void drawPlayer() {
        batch.setProjectionMatrix(cameraController.getCamera().combined);
        batch.begin();
        batch.draw(sprite.getTexture(), location.x, location.y);
        batch.end();
    }
    
    public void eat(Consumable consumable) {
        consumable.setConsumed(true);
        size = size.add(consumable.getSize());
        grow();
        cameraController.updateZoom(consumable.getSize().x * Constants.CAMERA_ZOOM_COEFICIENT);
    }
    
    private void grow() {
        createTexture();
    }
    
    private void shrink() {
        shrinkCountdown++;
        
        if (shrinkCountdown % Constants.PLAYER_SHRINK_COEFICIENT == 0) {
            size = size.sub(new GridPoint3(1, 1, 0));
            createTexture();
            shrinkCountdown = 0;
            cameraController.updateZoom(-Constants.CAMERA_ZOOM_COEFICIENT);
    
            if (size.x == 0 && size.y == 0) {
                alive = false;
            }
        }
        
    }
    
    private void createTexture() {
        Pixmap pixmap = new Pixmap(size.x, size.y, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        Texture texture = new Texture(pixmap);
        pixmap.dispose();
        
        this.texture = texture;
        sprite.setTexture(texture);
        sprite.setPosition(location.x, location.y);
        sprite.setRegion(texture);
        sprite.setBounds(location.x, location.y, size.x, size.y);
    }
    
    public Vector3 getLocation() {
        return location;
    }
    
    public void setLocation(Vector3 location) {
        this.location = location;
    }
    
    public GridPoint3 getSize() {
        return size;
    }
    
    public void setSize(GridPoint3 size) {
        this.size = size;
    }
    
    @Override
    public void dispose() {
        batch.dispose();
    }
    
    public Texture getTexture() {
        return texture;
    }
    
    public void setTexture(Texture texture) {
        this.texture = texture;
    }
    
    @Override
    public Sprite getSprite() {
        return sprite;
    }
    
    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }
    
    public boolean isAlive() {
        return alive;
    }
    
    public void setAlive(boolean alive) {
        this.alive = alive;
    }
    
    public static Player getInstance() {
        if (instance == null) {
            instance = new Player();
        }
        return instance;
    }
}
