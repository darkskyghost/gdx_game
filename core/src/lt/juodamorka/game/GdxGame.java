package lt.juodamorka.game;

import com.badlogic.gdx.Game;
import lt.juodamorka.game.screens.MenuScreen;

public class GdxGame extends Game {

    @Override
    public void create() {
        this.setScreen(new MenuScreen(this));
    }
    
    @Override
    public void render() {
        super.render();
    }
    
    @Override
    public void dispose() {
    }
    
    @Override
    public void resize(int width, int height) {
    }
    
    @Override
    public void pause() {
    
    }
    
    @Override
    public void resume() {
    
    }
}
