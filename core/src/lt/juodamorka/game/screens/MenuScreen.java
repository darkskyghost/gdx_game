package lt.juodamorka.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import lt.juodamorka.game.GdxGame;
import lt.juodamorka.game.controllers.camera.CameraController;
import lt.juodamorka.game.utils.Constants;

public class MenuScreen implements Screen {
    
    private GdxGame game;
    
    private SpriteBatch batch;
    private BitmapFont font;
    
    private CameraController cameraController;
    
    public MenuScreen(GdxGame game) {
        this.game = game;
        this.cameraController = CameraController.getInstance();
        
        batch = new SpriteBatch();
        font = new BitmapFont();
    }
    
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
        batch.setProjectionMatrix(cameraController.getCamera().combined);
        
        cameraController.update();
        
        batch.begin();
        font.draw(batch, "START", Constants.WORLD_WIDTH / 2, Constants.WORLD_HEIGHT / 2);
        batch.end();
        
        if (Gdx.input.isTouched()) {
            game.setScreen(new GameScreen(game));
            dispose();
        }
    }
    
    @Override
    public void show() {
    
    }
    
    @Override
    public void resize(int width, int height) {
    
    }
    
    @Override
    public void pause() {
    
    }
    
    @Override
    public void resume() {
    
    }
    
    @Override
    public void hide() {
    
    }
    
    @Override
    public void dispose() {
        batch.dispose();
        font.dispose();
    }
}
