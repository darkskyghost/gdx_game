package lt.juodamorka.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import lt.juodamorka.game.GdxGame;
import lt.juodamorka.game.controllers.camera.CameraController;
import lt.juodamorka.game.levels.Level;
import lt.juodamorka.game.levels.Level01;
import lt.juodamorka.game.objects.characters.player.Player;
import lt.juodamorka.game.utils.Constants;

public class GameScreen implements Screen {
    
    private GdxGame game;
    
    private Player player;
    
    private Level currentLevel;
    
    private CameraController cameraController;
    
    private SpriteBatch batch;
    private BitmapFont font;
    
    public GameScreen(GdxGame game) {
        this.game = game;
        
        player = Player.getInstance();
        currentLevel = new Level01(this.game);
        cameraController = CameraController.getInstance();
    
        batch = new SpriteBatch();
        font = new BitmapFont();
    }
    
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    
        currentLevel.update();
    
        if (player.isAlive()) {
            player.update();
            cameraController.update();
        }
    
        checkForGameOver();
    }
    
    private void checkForGameOver() {
        if (!player.isAlive()) {
            batch.begin();
            Gdx.gl.glClearColor(0, 0, 0, 1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            font.draw(batch, "YOU HAVE DIED", Constants.WORLD_WIDTH / 2, Constants.WORLD_HEIGHT / 2);
            batch.end();
        
            if (Gdx.input.isTouched()) {
                game.setScreen(new GameScreen(game));
                dispose();
                player.respawn();
            }
        }
    }
    
    @Override
    public void show() {
    
    }
    
    @Override
    public void resize(int width, int height) {
    
    }
    
    @Override
    public void pause() {
    
    }
    
    @Override
    public void resume() {
    
    }
    
    @Override
    public void hide() {
    
    }
    
    @Override
    public void dispose() {
        batch.dispose();
        font.dispose();
    }
}
