package lt.juodamorka.game.utils;

public class Constants {
    
    public static final int WORLD_WIDTH = 1000;
    public static final int WORLD_HEIGHT = 1000;
    
    public static final int PLAYER_MOVEMENT_MULTIPLIER = 200;
    public static final int PLAYER_SHRINK_COEFICIENT = 1000;
    
    public static final float CAMERA_ZOOM_COEFICIENT = 0.1F;
}
