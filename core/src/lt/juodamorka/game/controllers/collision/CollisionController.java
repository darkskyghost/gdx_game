package lt.juodamorka.game.controllers.collision;

import lt.juodamorka.game.controllers.GenericController;
import lt.juodamorka.game.objects.GameObject;

public class CollisionController implements GenericController {
    
    private static CollisionController instance;
    
    private CollisionController() {
    }
    
    @Override
    public void update() {
    }
    
    public boolean isTouching(GameObject object1, GameObject object2) {
        return object1.getSprite().getBoundingRectangle().overlaps(object2.getSprite().getBoundingRectangle());
    }
    
    public static CollisionController getInstance() {
        if (instance == null) {
            instance = new CollisionController();
        }
        return instance;
    }
}
