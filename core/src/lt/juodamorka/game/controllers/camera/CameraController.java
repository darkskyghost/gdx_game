package lt.juodamorka.game.controllers.camera;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import lt.juodamorka.game.controllers.MovementController;
import lt.juodamorka.game.utils.Constants;

public class CameraController implements MovementController {
    
    private static CameraController instance;
    
    private OrthographicCamera camera;
    
    private CameraController() {
        camera = new OrthographicCamera(Constants.WORLD_WIDTH, Constants.WORLD_HEIGHT);
        camera.position.set(Constants.WORLD_WIDTH / 2, Constants.WORLD_HEIGHT / 2, 0);
    }
    
    @Override
    public void update() {
        processMovementInput();
        camera.update();
    }
    
    public void updateZoom(float zoom) {
        camera.zoom += zoom;
    }
    
    @Override
    public void processMovementInput() {
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            camera.translate(-(Constants.PLAYER_MOVEMENT_MULTIPLIER * Gdx.graphics.getDeltaTime()), 0, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            camera.translate(+(Constants.PLAYER_MOVEMENT_MULTIPLIER * Gdx.graphics.getDeltaTime()), 0, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            camera.translate(0, +(Constants.PLAYER_MOVEMENT_MULTIPLIER * Gdx.graphics.getDeltaTime()), 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            camera.translate(0, -(Constants.PLAYER_MOVEMENT_MULTIPLIER * Gdx.graphics.getDeltaTime()), 0);
        }
    
        camera.position.x = MathUtils.clamp(camera.position.x, -Constants.WORLD_WIDTH, Constants.WORLD_WIDTH);
        camera.position.y = MathUtils.clamp(camera.position.y, -Constants.WORLD_HEIGHT, Constants.WORLD_HEIGHT);
    }
    
    public static CameraController getInstance() {
        if (instance == null) {
            instance = new CameraController();
        }
        return instance;
    }
    
    public OrthographicCamera getCamera() {
        if (instance == null) {
            instance = new CameraController();
        }
        
        return camera;
    }
}
