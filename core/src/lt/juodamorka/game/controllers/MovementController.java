package lt.juodamorka.game.controllers;

public interface MovementController extends GenericController {
    
    void processMovementInput();
}
