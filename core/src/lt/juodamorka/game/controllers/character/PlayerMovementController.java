package lt.juodamorka.game.controllers.character;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.MathUtils;
import lt.juodamorka.game.controllers.MovementController;
import lt.juodamorka.game.objects.characters.player.Player;
import lt.juodamorka.game.utils.Constants;

public class PlayerMovementController implements CharacterController, MovementController {
    
    private Player player;
    
    public PlayerMovementController(Player player) {
        this.player = player;
    }
    
    @Override
    public void update() {
        processMovementInput();
    }
    
    @Override
    public void processMovementInput() {
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            player.getLocation().x -= Constants.PLAYER_MOVEMENT_MULTIPLIER * Gdx.graphics.getDeltaTime();
            player.getSprite().setPosition(player.getSprite().getX() - Constants.PLAYER_MOVEMENT_MULTIPLIER * Gdx.graphics.getDeltaTime(), player.getSprite().getY());
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            player.getLocation().x += Constants.PLAYER_MOVEMENT_MULTIPLIER * Gdx.graphics.getDeltaTime();
            player.getSprite().setPosition(player.getSprite().getX() + Constants.PLAYER_MOVEMENT_MULTIPLIER * Gdx.graphics.getDeltaTime(), player.getSprite().getY());
        }
        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            player.getLocation().y += Constants.PLAYER_MOVEMENT_MULTIPLIER * Gdx.graphics.getDeltaTime();
            player.getSprite().setPosition(player.getSprite().getX(), player.getSprite().getY() + Constants.PLAYER_MOVEMENT_MULTIPLIER * Gdx.graphics.getDeltaTime());
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            player.getLocation().y -= Constants.PLAYER_MOVEMENT_MULTIPLIER * Gdx.graphics.getDeltaTime();
            player.getSprite().setPosition(player.getSprite().getX(), player.getSprite().getY() - Constants.PLAYER_MOVEMENT_MULTIPLIER * Gdx.graphics.getDeltaTime());
        }
    
        player.getLocation().x = MathUtils.clamp(player.getLocation().x, -Constants.WORLD_WIDTH, Constants.WORLD_WIDTH);
        player.getLocation().y = MathUtils.clamp(player.getLocation().y, -Constants.WORLD_HEIGHT, Constants.WORLD_HEIGHT);
    }
}
