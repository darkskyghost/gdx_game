package lt.juodamorka.game.controllers.character;

import lt.juodamorka.game.controllers.GenericController;

public interface CharacterController extends GenericController {
    
    void update();
}
