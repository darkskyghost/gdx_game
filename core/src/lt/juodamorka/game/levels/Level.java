package lt.juodamorka.game.levels;

public interface Level {
    
    void update();
    
    void dispose();
}
