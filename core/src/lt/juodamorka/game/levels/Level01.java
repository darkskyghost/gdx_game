package lt.juodamorka.game.levels;

import lt.juodamorka.game.GdxGame;
import lt.juodamorka.game.controllers.collision.CollisionController;
import lt.juodamorka.game.objects.characters.player.Player;
import lt.juodamorka.game.objects.consumables.food.Glucose;
import lt.juodamorka.game.objects.consumables.food.Protein;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Level01 implements Level {
    
    private GdxGame game;
    private Player player;
    private CollisionController collisionController;
    
    private List<Glucose> glucoseList;
    private List<Protein> proteinList;
    
    private int glucoseCount = 0;
    private int proteinCount = 0;
    
    private int randomGlucoseCount = new Random().nextInt(100);
    private int randomProteinCount = new Random().nextInt(20);
    
    public Level01(GdxGame game) {
        this.game = game;
        this.player = Player.getInstance();
        this.collisionController = CollisionController.getInstance();
        glucoseList = new ArrayList<>();
        proteinList = new ArrayList<>();
        
        while (glucoseCount <= randomGlucoseCount) {
            glucoseList.add(new Glucose());
            glucoseCount++;
        }
    }
    
    @Override
    public void update() {
        glucoseList.forEach(food -> {
            food.update();
            if (collisionController.isTouching(player, food)) {
                player.eat(food);
            }
        });
    
        proteinList.forEach(food -> {
            food.update();
            if (collisionController.isTouching(player, food)) {
                player.eat(food);
            }
        });
        
        List<Glucose> consumedGlucose = glucoseList.stream().filter(Glucose::getConsumed).collect(Collectors.toList());
        int consumedGlucoseCount = consumedGlucose.size();
        
        glucoseCount = glucoseCount - consumedGlucoseCount;
        
        glucoseList.removeAll(consumedGlucose);
    
        if (consumedGlucoseCount > 0) {
            for (int added = 0; added <= consumedGlucoseCount; added++) {
                glucoseList.add(new Glucose());
                glucoseCount++;
            }
        }
        
        if (player.getSize().x >= 20 && player.getSize().y >= 20) {
            if (proteinList.isEmpty()) {
                while (proteinCount <= randomProteinCount) {
                    proteinList.add(new Protein());
                    proteinCount++;
                }
            }
        }
        
        if (!proteinList.isEmpty()) {
            List<Protein> consumedProtein = proteinList.stream().filter(Protein::getConsumed).collect(Collectors.toList());
            int consumedProteinCount = consumedProtein.size();
    
            proteinCount = proteinCount - consumedProteinCount;
    
            proteinList.removeAll(consumedProtein);
    
            if (consumedProteinCount > 0) {
                for (int added = 0; added <= consumedProteinCount; added++) {
                    proteinList.add(new Protein());
                    proteinCount++;
                }
            }
        }
    }
    
    @Override
    public void dispose() {

    }
}
